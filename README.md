The Pulse app concept with restricted functionality illustrates that consent request templates could be applied in different app contexts.

Creative Commons Attribution-ShareAlike 4.0 International License
