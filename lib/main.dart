import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'apps/activity/activity_app.dart';
import 'apps/challenge/challenge_app.dart';
import 'apps/consent/consent_app.dart';
import 'apps/home/home_app.dart';
import 'apps/profile/profile_app.dart';
import 'providers/activities_provider.dart';
import 'providers/app_provider.dart';
import 'providers/challenges_provider.dart';
import 'screens/not_found_screen.dart';
import 'widgets/app_bar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: AppProvider()),
          ChangeNotifierProvider.value(value: ActivitiesProvider()),
          ChangeNotifierProvider.value(value: ChallengesProvider()),
        ],
        child: HomePage(),
      ),
    );
  }
}

class HomePage extends StatelessWidget {
  final PageStorageBucket bucket = PageStorageBucket();

  Widget _getPage(int index) {
    switch (index) {
      case 0:
        return HomeApp(key: PageStorageKey('HomeApp'));
      case 1:
        return ActivityApp(key: PageStorageKey('ActivityApp'));
      case 2:
        return ChallengeApp(key: PageStorageKey('ChallengeApp'));
      case 3:
        return ProfileApp(key: PageStorageKey('ProfileApp'));
      case 4:
        return ConsentApp(key: PageStorageKey('ConsentApp'));
    }

    return NotFoundScreen('global');
  }

  @override
  Widget build(BuildContext ctx) {
    return FutureBuilder(
      future: Provider.of<AppProvider>(ctx, listen: false).fetchAll(ctx),
      builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
          ? Center(child: CircularProgressIndicator())
          : Consumer<AppProvider>(
              builder: (ctx, app, ch) => Scaffold(
                body: SafeArea(
                  child: PageStorage(
                    child: _getPage(app.currentIndex),
                    bucket: bucket,
                  ),
                ),
                bottomNavigationBar:
                    app.navBarAvailable ? AppButtonBar() : null,
              ),
            ),
    );
  }
}
