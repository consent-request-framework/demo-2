import 'dart:async';
import 'dart:convert';

import 'package:demo2/enums/activity_type_enum.dart';
import 'package:demo2/enums/challenge_status_enum.dart';
import 'package:demo2/models/challenge.dart';
import 'package:demo2/models/stat.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class ChallengesProvider with ChangeNotifier {
  List<Challenge> _items = [];

  List<Challenge> getItems() {
    return [..._items];
  }

  Challenge getItemAt(int index) {
    return _items[index];
  }

  Future<void> fetchAll(ctx) async {
    _items = [];

    final data = await rootBundle.loadString('assets/jsons/challenges.json');

    for (final jsonItem in json.decode(data)) {
      final item = Challenge();
      final dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
      item.dateFrom = DateFormat(dateFormat).parse(jsonItem['dateFrom']);
      item.dateTo = DateFormat(dateFormat).parse(jsonItem['dateTo']);
      item.status = _getStatus(jsonItem['status']);
      item.type = _getType(jsonItem['type']);
      item.name = jsonItem['name'];
      item.icon = _getIcon(item.type);
      List<Stat> stats = List();
      for (final jsonItemStat in jsonItem['stat']) {
        final stat = Stat();
        stat.user = jsonItemStat['user'];
        stat.score = jsonItemStat['score'];
        stat.isYou = jsonItemStat['isYou'];
        stats.add(stat);
      }
      item.stats = stats;
      _items.add(item);
    }

    _items.sort((a, b) {
      return b.dateFrom.compareTo(a.dateFrom);
    });

    notifyListeners();
  }

  ActivityTypeEnum _getType(String value) {
    if (value == describeEnum(ActivityTypeEnum.running)) {
      return ActivityTypeEnum.running;
    }
    if (value == describeEnum(ActivityTypeEnum.swimming)) {
      return ActivityTypeEnum.swimming;
    }
    if (value == describeEnum(ActivityTypeEnum.skiing)) {
      return ActivityTypeEnum.skiing;
    }
    return null;
  }

  ChallengeStatusEnum _getStatus(String value) {
    if (value == describeEnum(ChallengeStatusEnum.inProgress)) {
      return ChallengeStatusEnum.inProgress;
    }
    if (value == describeEnum(ChallengeStatusEnum.completed)) {
      return ChallengeStatusEnum.completed;
    }
    return null;
  }

  IconData _getIcon(ActivityTypeEnum type) {
    if (type == ActivityTypeEnum.running) {
      return Icon(FontAwesomeIcons.running).icon;
    }
    if (type == ActivityTypeEnum.swimming) {
      return Icon(FontAwesomeIcons.swimmer).icon;
    }
    if (type == ActivityTypeEnum.skiing) {
      return Icon(FontAwesomeIcons.skiing).icon;
    }
    return null;
  }
}
