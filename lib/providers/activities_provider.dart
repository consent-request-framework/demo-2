import 'dart:async';
import 'dart:convert';

import 'package:demo2/enums/activity_type_enum.dart';
import 'package:demo2/models/activity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class ActivitiesProvider with ChangeNotifier {
  List<Activity> _items = [];

  List<Activity> getItems() {
    return [..._items];
  }

  Activity getItemAt(int index) {
    return _items[index];
  }

  Future<void> fetchAll(ctx) async {
    _items = [];

    final data = await rootBundle.loadString('assets/jsons/activities.json');

    for (final jsonItem in json.decode(data)) {
      final item = Activity();
      item.pulse = jsonItem['pulse'];
      item.date =
          DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(jsonItem['date']);
      final type = jsonItem['type'];
      item.type = _getEnum(type);
      item.name = '${type[0].toUpperCase()}${type.substring(1)}';
      item.icon = _getIcon(item.type);
      _items.add(item);
    }

    _items.sort((a,b) {
      return b.date.compareTo(a.date);
    });

    notifyListeners();
  }

  ActivityTypeEnum _getEnum(String value) {
    if (value == describeEnum(ActivityTypeEnum.running)) {
      return ActivityTypeEnum.running;
    }
    if (value == describeEnum(ActivityTypeEnum.swimming)) {
      return ActivityTypeEnum.swimming;
    }
    if (value == describeEnum(ActivityTypeEnum.skiing)) {
      return ActivityTypeEnum.skiing;
    }
    return null;
  }

  IconData _getIcon(ActivityTypeEnum type) {
    if (type == ActivityTypeEnum.running) {
      return Icon(FontAwesomeIcons.running).icon;
    }
    if (type == ActivityTypeEnum.swimming) {
      return Icon(FontAwesomeIcons.swimmer).icon;
    }
    if (type == ActivityTypeEnum.skiing) {
      return Icon(FontAwesomeIcons.skiing).icon;
    }
    return null;
  }
}
