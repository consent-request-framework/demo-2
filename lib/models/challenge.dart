import 'package:demo2/enums/activity_type_enum.dart';
import 'package:demo2/enums/challenge_status_enum.dart';
import 'package:flutter/material.dart';

import 'stat.dart';

class Challenge {
  int id;
  ActivityTypeEnum type;
  String name;
  ChallengeStatusEnum status;
  DateTime dateFrom;
  DateTime dateTo;
  IconData icon;
  List<Stat> stats;
}
