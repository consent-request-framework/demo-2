import 'package:demo2/enums/activity_type_enum.dart';
import 'package:flutter/material.dart';

class Activity {
  ActivityTypeEnum type;
  DateTime date;
  int pulse;
  String name;
  IconData icon;
}
