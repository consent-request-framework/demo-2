import 'package:demo2/common/consent_widget.dart';
import 'package:demo2/models/activity.dart';
import 'package:demo2/providers/activities_provider.dart';
import 'package:demo2/widgets/avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ActivityListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    Widget _itemBuilder(BuildContext context, Activity item) {
      return ListTile(
        leading: Container(
          width: 55,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.withOpacity(0.2)),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          alignment: Alignment.center,
          child: Icon(item.icon, color: Colors.indigoAccent),
        ),
        title: Text(item.name),
        subtitle: Text(DateFormat.yMd().format(item.date)),
        trailing: Container(
          width: 90,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                item.pulse.toString(),
                style: TextStyle(
                  color: Colors.purple,
                  fontWeight: FontWeight.bold,
                  fontSize: 24,
                ),
              ),
              Icon(FontAwesomeIcons.heartbeat, size: 13, color: Colors.purple)
            ],
          ),
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Activities'),
        actions: <Widget>[Avatar()],
      ),
      body: FutureBuilder(
        future:
            Provider.of<ActivitiesProvider>(ctx, listen: false).fetchAll(ctx),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: CircularProgressIndicator())
            : Consumer<ActivitiesProvider>(
                builder: (ctx, app, ch) => ConsentWidget(
                  noConsentWidget: Center(
                    child: Text(
                      'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                      textAlign: TextAlign.center,
                    ),
                  ),
                  purposeIds: [1, 2, 3, 4, 5, 6],
                  widget: ListView.separated(
                    padding: const EdgeInsets.all(5),
                    itemCount: app.getItems().length,
                    itemBuilder: (context, index) =>
                        _itemBuilder(context, app.getItemAt(index)),
                    separatorBuilder: (context, index) => Divider(),
                  ),
                ),
              ),
      ),
    );
  }
}
