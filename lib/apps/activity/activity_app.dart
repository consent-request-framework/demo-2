import 'package:demo2/apps/activity/screens/activity_list_screen.dart';
import 'package:demo2/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

class ActivityApp extends StatelessWidget {
  const ActivityApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case ActivityListScreen.routeName:
                return ActivityListScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
