import 'package:demo2/common/consent_widget.dart';
import 'package:demo2/providers/app_provider.dart';
import 'package:demo2/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final app = Provider.of<AppProvider>(context, listen: false);
    final _profile = app.profile;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Profile'),
        actions: <Widget>[Avatar()],
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () =>
              {Provider.of<AppProvider>(context, listen: false).goBack()},
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Form(
              key: _formKey,
              child: ListView(
                shrinkWrap: true,
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                children: [
                  SizedBox(height: 10),
                  ConsentWidget(
                    noConsentWidget: Center(
                        child: Text(
                      'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                      textAlign: TextAlign.center,
                    )),
                    purposeIds: [1, 3, 4],
                    widget: TextFormField(
                      onSaved: (String value) => _profile.name = value,
                      initialValue: _profile.name,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Name',
                        hintText: 'Full Name',
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  ConsentWidget(
                    noConsentWidget: Center(
                        child: Text(
                      'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                      textAlign: TextAlign.center,
                    )),
                    purposeIds: [1, 3, 4],
                    widget: TextFormField(
                      onSaved: (String value) => _profile.district = value,
                      initialValue: _profile.district,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'District',
                        hintText: 'District',
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  ConsentWidget(
                    noConsentWidget: Center(
                        child: Text(
                      'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                      textAlign: TextAlign.center,
                    )),
                    purposeIds: [1, 2, 3, 4, 5, 6],
                    widget: TextFormField(
                      keyboardType: TextInputType.number,
                      onSaved: (String value) =>
                          _profile.age = int.tryParse(value),
                      initialValue:
                          _profile.age != null ? '${_profile.age}' : '',
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Age',
                        hintText: 'Age',
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  ConsentWidget(
                    noConsentWidget: Center(
                        child: Text(
                      'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                      textAlign: TextAlign.center,
                    )),
                    purposeIds: [1, 2, 3, 4, 5, 6],
                    widget: TextFormField(
                      keyboardType: TextInputType.number,
                      onSaved: (String value) =>
                          _profile.weight = double.tryParse(value),
                      initialValue:
                          _profile.weight != null ? '${_profile.weight}' : '',
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Weight',
                        hintText: 'Weight',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          OutlineButton(
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                app.profile = _profile;
                app.goBack();
              }
            },
            child: const Text('Save'),
          )
        ],
      ),
    );
  }
}
