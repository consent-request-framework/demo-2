import 'package:demo2/apps/profile/screens/profile_screen.dart';
import 'package:demo2/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

class ProfileApp extends StatelessWidget {
  const ProfileApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case '/':
                return ProfileScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
