import 'package:demo2/common/consent_widget.dart';
import 'package:demo2/enums/challenge_status_enum.dart';
import 'package:demo2/models/challenge.dart';
import 'package:demo2/providers/challenges_provider.dart';
import 'package:demo2/widgets/avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'stat_screen.dart';

class ChallengeListScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext ctx) {
    Widget _itemBuilder(BuildContext context, Challenge item) {
      return ListTile(
        leading: Container(
          width: 55,
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.withOpacity(0.2)),
            borderRadius: BorderRadius.all(Radius.circular(5.0)),
          ),
          alignment: Alignment.center,
          child: Icon(item.icon, color: Colors.indigoAccent),
        ),
        title: Text(item.name),
        subtitle: Text(
          '${DateFormat.yMd().format(item.dateFrom)} - ${DateFormat.yMd().format(item.dateTo)}',
        ),
        trailing: Container(
          width: 55,
          child: item.status == ChallengeStatusEnum.inProgress
              ? Icon(FontAwesomeIcons.syncAlt, color: Colors.orange)
              : Icon(FontAwesomeIcons.check, color: Colors.green),
        ),
        onTap: () {
          Navigator.pushNamed(
            context,
            StatScreen.routeName,
            arguments: item,
          );
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Challenges'),
        actions: <Widget>[Avatar()],
      ),
      body: FutureBuilder(
        future:
            Provider.of<ChallengesProvider>(ctx, listen: false).fetchAll(ctx),
        builder: (ctx, snp) => snp.connectionState == ConnectionState.waiting
            ? Center(child: CircularProgressIndicator())
            : Consumer<ChallengesProvider>(
                builder: (ctx, app, ch) => ConsentWidget(
                  noConsentWidget: Center(
                      child: Text(
                    'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                    textAlign: TextAlign.center,
                  )),
                  purposeIds: [1, 2, 3, 4],
                  useMinCase: true,
                  widget: ListView.separated(
                    padding: const EdgeInsets.all(5),
                    itemCount: app.getItems().length,
                    itemBuilder: (context, index) =>
                        _itemBuilder(context, app.getItemAt(index)),
                    separatorBuilder: (context, index) => Divider(),
                  ),
                ),
              ),
      ),
    );
  }
}
