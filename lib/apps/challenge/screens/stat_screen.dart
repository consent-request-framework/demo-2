import 'package:demo2/models/challenge.dart';
import 'package:demo2/models/stat.dart';
import 'package:demo2/widgets/avatar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StatScreen extends StatelessWidget {
  static const routeName = '/stat';

  @override
  Widget build(BuildContext ctx) {
    final Challenge item = ModalRoute.of(ctx).settings.arguments;
    final List<Stat> stats = item.stats;

    Widget _itemBuilder(BuildContext context, Stat item) {
      final place = item.isYou ? '#${item.user} You' : '#${item.user}';
      final width = MediaQuery.of(context).size.width;
      final scoreWidth = width * item.score / 100;
      final height = 20.0;
      final margin = 10.0;
      final barPadding = 0.0;
      return Align(
        alignment: Alignment.centerLeft,
        child: Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(left: barPadding),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: margin),
                width: scoreWidth,
                height: height,
                decoration: BoxDecoration(
                  color: item.isYou
                      ? Colors.deepPurpleAccent
                      : Colors.indigoAccent,
                  border: Border.all(color: Colors.grey.withOpacity(0.2)),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(45.0),
                    bottomRight: Radius.circular(45.0),
                  ),
                ),
                alignment: Alignment.centerLeft,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: margin),
              height: height,
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Text(
                  place,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: margin),
              height: height,
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(left: scoreWidth + barPadding + 3),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${item.score}',
                      style: TextStyle(
                        color: Colors.purple,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                    Icon(
                      FontAwesomeIcons.heartbeat,
                      size: 11,
                      color: Colors.purple,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(item.name),
        actions: <Widget>[Avatar()],
      ),
      body: Column(
        children: [
          SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                FontAwesomeIcons.calendarAlt,
                size: 25,
                color: Colors.red,
              ),
              SizedBox(width: 5),
              Text(
                'Only 10 days left!',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(height: 10),
          Expanded(
            child: ListView.builder(
              itemCount: stats.length,
              itemBuilder: (context, index) =>
                  _itemBuilder(context, stats[index]),
            ),
          )
        ],
      ),
    );
  }
}
