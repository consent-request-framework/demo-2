import 'package:demo2/screens/not_found_screen.dart';
import 'package:flutter/material.dart';

import 'screens/challenge_list_screen.dart';
import 'screens/stat_screen.dart';

class ChallengeApp extends StatelessWidget {
  const ChallengeApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case ChallengeListScreen.routeName:
                return ChallengeListScreen();
              case StatScreen.routeName:
                return StatScreen();
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
