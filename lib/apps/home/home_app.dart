import 'package:demo2/common/consent_widget.dart';
import 'package:demo2/screens/not_found_screen.dart';
import 'package:demo2/widgets/avatar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeApp extends StatefulWidget {
  const HomeApp({Key key}) : super(key: key);

  @override
  _HomeAppState createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  int indexTab = 0;

  @override
  Widget build(BuildContext context) {
    return Navigator(
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          settings: settings,
          builder: (BuildContext context) {
            switch (settings.name) {
              case '/':
                return DefaultTabController(
                  initialIndex: 0,
                  length: 2,
                  child: Scaffold(
                    appBar: AppBar(
                      centerTitle: true,
                      actions: <Widget>[Avatar()],
                      title: Text('Pulse'),
                      bottom: TabBar(
                        onTap: (index) {
                          setState(() {
                            indexTab = index;
                          });
                        },
                        indicator: BoxDecoration(color: Colors.white),
                        labelColor: Colors.purple,
                        unselectedLabelColor: Colors.white,
                        tabs: <Widget>[
                          Tab(icon: Icon(FontAwesomeIcons.userAlt)),
                          Tab(icon: Icon(FontAwesomeIcons.users)),
                        ],
                      ),
                    ),
                    floatingActionButton: indexTab == 0
                        ? FloatingActionButton(
                            onPressed: () {
                              showModalBottomSheet<void>(
                                context: context,
                                builder: (BuildContext context) {
                                  return Container(
                                    height: 400,
                                    color: Colors.white,
                                    child: Center(
                                      child: ConsentWidget(
                                        noConsentWidget: Text(
                                          'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                                          textAlign: TextAlign.center,
                                        ),
                                        purposeIds: [1, 2, 3, 4],
                                        useMinCase: true,
                                        widget: Image(
                                          image: AssetImage(
                                              'assets/images/qrcode.png'),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            },
                            child: Icon(FontAwesomeIcons.qrcode),
                          )
                        : Container(),
                    body: TabBarView(
                      children: <Widget>[
                        Column(
                          children: [
                            ConsentWidget(
                              noConsentWidget: Image(
                                image: AssetImage('assets/images/map3.png'),
                              ),
                              purposeIds: [1, 2, 3, 4, 5, 6],
                              widget: Image(
                                image: AssetImage('assets/images/map1.png'),
                              ),
                            ),
                            Expanded(
                              child: ConsentWidget(
                                noConsentWidget: Center(
                                  child: Text(
                                    'You have not provided your consent for data processing. Please update you consent to be able to use this feature.',
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                purposeIds: [1, 2, 3, 4, 5, 6],
                                widget: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: 130,
                                      height: 130,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Colors.purple.withOpacity(0.5),
                                            spreadRadius: 5,
                                            blurRadius: 7,
                                            offset: Offset(0, 0),
                                          ),
                                        ],
                                        borderRadius:
                                            BorderRadius.circular(100),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '126',
                                            style: TextStyle(
                                              fontSize: 47,
                                              color: Colors.purple,
                                            ),
                                          ),
                                          Icon(
                                            FontAwesomeIcons.heartbeat,
                                            size: 21,
                                            color: Colors.purple,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Image(image: AssetImage('assets/images/map2.png')),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 130,
                                    height: 130,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.purple.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0, 0),
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(100),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          '2136',
                                          style: TextStyle(
                                            fontSize: 44,
                                            color: Colors.purple,
                                          ),
                                        ),
                                        Icon(
                                          FontAwesomeIcons.heartbeat,
                                          size: 19,
                                          color: Colors.purple,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
            }
            return NotFoundScreen(settings.name);
          },
        );
      },
    );
  }
}
