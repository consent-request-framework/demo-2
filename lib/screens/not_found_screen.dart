import 'package:flutter/material.dart';

class NotFoundScreen extends StatelessWidget {
  final String routeName;

  const NotFoundScreen(this.routeName, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Not Found'),
      ),
      body: Center(
        child: const Text('Not Found'),
      ),
    );
  }
}
